type [@state] state = {
  accounts: int array;
} (*@ invariant forall i. 0 <= i < Array.length accounts -> accounts.(i) >= 0 *)

(*@ predicate eq (s1 s2: state) =
      Array.length s1.accounts = Array.length s2.accounts &&
      forall i. 0 <= i < Array.length s1.accounts ->
        s1.accounts.(i) = s2.accounts.(i) *)

let deposit i v state =
  state.accounts.(i) <- state.accounts.(i) + v
(*@ deposit i v state
      requires 0 <= v
      requires 0 <= i < Array.length state.accounts
      ensures  state.accounts.(i) = (old state).accounts.(i) + v
      ensures  forall j. 0 <= j < Array.length state.accounts -> i <> j ->
                 state.accounts.(j) = (old state).accounts.(j) *)

let withdraw i v state =
  state.accounts.(i) <- state.accounts.(i) - v
(*@ withdraw i v state
      requires 0 <= v
      requires state.accounts.(i) - v >= 0
      requires 0 <= i < Array.length state.accounts
      ensures  state.accounts.(i) = (old state).accounts.(i) - v
      ensures  forall j. 0 <= j < Array.length state.accounts -> i <> j ->
                 state.accounts.(j) = (old state).accounts.(j) *)
