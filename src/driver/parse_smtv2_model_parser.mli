
(* The type of tokens. *)

type token = 
  | UNDERSCORE
  | TRUE
  | STRING of (string)
  | STORE
  | RPAREN
  | NOT
  | MODEL
  | MINUS_INT_STR of (string)
  | MINUS_DEC_STR of (string * string)
  | LPAREN
  | LET
  | LE
  | LAMBDA
  | ITE
  | INT_TO_BV of (string)
  | INT_STR of (string)
  | GE
  | FORALL
  | FLOAT_VALUE of (Model_parser.float_type)
  | FLOAT_TYPE of (string * string)
  | FALSE
  | EQUAL
  | EOF
  | DIV
  | DEFINE_FUN
  | DEC_STR of (string * string)
  | DECLARE_SORT
  | DECLARE_FUN
  | DECLARE_DATATYPES
  | CONST
  | COMMENT of (string)
  | BITVECTOR_VALUE_SHARP of (string)
  | BITVECTOR_VALUE_INT of (string)
  | BITVECTOR_TYPE
  | BITVECTOR_EXTRACT of (string)
  | ATOM of (string)
  | AS_ARRAY
  | AS
  | ARRAY_LAMBDA
  | AND

(* This exception is raised by the monolithic API functions. *)

exception Error

(* The monolithic API. *)

val output: (Lexing.lexbuf -> token) -> Lexing.lexbuf -> (Smt2_model_defs.definition Wstdlib.Mstr.t)

module MenhirInterpreter : sig
  
  (* The incremental API. *)
  
  include MenhirLib.IncrementalEngine.INCREMENTAL_ENGINE
    with type token = token
  
end

(* The entry point(s) to the incremental API. *)

module Incremental : sig
  
  val output: Lexing.position -> (Smt2_model_defs.definition Wstdlib.Mstr.t) MenhirInterpreter.checkpoint
  
end
