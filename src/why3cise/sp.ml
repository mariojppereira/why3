open While_ast
open Why3

let counter = ref 0
   
let rec id_change expr to_change id =
  match expr.expr_desc with
  | Eseq (e1,e2) ->
     let n_e1 = id_change e1 to_change id in
     let n_e2 = id_change e2 to_change id in
     mk_expr (Eseq (n_e1,n_e2)) Loc.dummy_position
  | Eident ident ->
     if to_change = ident.id_str then
       let n_id = mk_id Loc.dummy_position id in
       mk_expr (Eident n_id) Loc.dummy_position
     else expr
  | Eif (e1,e2,e3) ->
     let n_e1 = id_change e1 to_change id in
     let n_e2 = id_change e2 to_change id in
     let n_e3 = id_change e3 to_change id in
     mk_expr (Eif (n_e1,n_e2,n_e3)) Loc.dummy_position
  | Eand (e1,e2) ->
     let n_e1 = id_change e1 to_change id in
     let n_e2 = id_change e2 to_change id in
     mk_expr (Eand (n_e1,n_e2)) Loc.dummy_position
  | Eor (e1,e2) ->
     let n_e1 = id_change e1 to_change id in
     let n_e2 = id_change e2 to_change id in
     mk_expr (Eor (n_e1,n_e2)) Loc.dummy_position
  | Enot e ->
     let n_e = id_change e to_change id in
     mk_expr (Enot n_e) Loc.dummy_position
  | Eexists (ident,e) ->
     let id_ref = ref ident in
     if to_change = ident.id_str then
       id_ref := mk_id Loc.dummy_position to_change;
     let n_e = id_change e to_change id in
     mk_expr (Eexists (!id_ref, n_e)) Loc.dummy_position
  | Eequals (e1,e2) ->
     let n_e1 = id_change e1 to_change id in
     let n_e2 = id_change e2 to_change id in
     mk_expr (Eequals (n_e1,n_e2)) Loc.dummy_position
  | Emath_app (e1, ident, e2) ->
     let n_e1 = id_change e1 to_change id in
     let n_e2 = id_change e2 to_change id in
     mk_expr (Emath_app (n_e1,ident,n_e2)) Loc.dummy_position
  | Elet (ident, e1 , e2) ->
     let aux =
       ref (mk_id Loc.dummy_position ident.id_str) in
     if to_change = ident.id_str then
       aux := mk_id Loc.dummy_position id;
     let n_e1 = id_change e1 to_change id in
     let n_e2 = id_change e2 to_change id in
     mk_expr (Elet (!aux,n_e1,n_e2)) Loc.dummy_position
  | Eapp (ident , e_list) ->
     let aux =
       ref (mk_id Loc.dummy_position ident.id_str) in
     if to_change = ident.id_str then
       aux := mk_id Loc.dummy_position id;
     let n_list = List.map (fun a ->
                      id_change a to_change id) e_list in
     mk_expr (Eapp (!aux, n_list)) Loc.dummy_position
  | Eassign (ident, e) ->
     let aux =
       ref (mk_id Loc.dummy_position ident.id_str) in
     if to_change = ident.id_str then
       aux := mk_id Loc.dummy_position id;
     let n_e = id_change e to_change id in
     mk_expr (Eassign (!aux, n_e)) Loc.dummy_position
  | Erecord_app (ident, id_list) ->
     let aux =
       ref (mk_id Loc.dummy_position ident.id_str) in
     if to_change = ident.id_str then
       aux := mk_id Loc.dummy_position id;
     let n_list =
       List.map (fun a ->
           if to_change = a.id_str then
             mk_id Loc.dummy_position id
           else a) id_list in
     mk_expr (Erecord_app (!aux, n_list)) Loc.dummy_position
  | Etuple e_list ->
     let n_list =
       List.map (fun a ->
           id_change a to_change id) e_list in
     mk_expr (Etuple n_list) Loc.dummy_position
  | _ -> expr
   
let rec sp expr pre =
  match expr.expr_desc with
  | Eskip -> pre
  | Eseq (e1,e2) ->
     sp e2 (sp e1 pre)
  | Eif (b,e1,e2) ->
     let sp1_pre = mk_expr (Eand (pre,b)) Loc.dummy_position in
     let not_b = mk_expr (Enot b) b.expr_loc in
     let sp2_pre = mk_expr (Eand (pre,not_b)) Loc.dummy_position in
     let sp1 = sp e1 sp1_pre in
     let sp2 = sp e2 sp2_pre in
     mk_expr (Eor (sp1, sp2)) Loc.dummy_position
  | Eassign (id, e) ->
     let id_s = id.id_str in
     let exists_id =
       mk_id Loc.dummy_position (("v")^(string_of_int !counter)) in
     incr counter;
     begin match e.expr_desc with
     | Eseq (e1,e2) ->
        let v_expr =
          mk_expr (Eident (mk_id Loc.dummy_position id_s)) Loc.dummy_position in
        let n_e = id_change e1 id_s exists_id.id_str in
        let equal_expr = mk_expr (Eequals (v_expr, n_e)) Loc.dummy_position in
        let n_pre = id_change pre id_s exists_id.id_str in
        let exists_expr =
          mk_expr (Eand (equal_expr, n_pre)) Loc.dummy_position in
        let next_expr = sp e2 pre in
        let exists_e = mk_expr (Eexists (exists_id, exists_expr)) Loc.dummy_position in
        mk_expr (Eand (exists_e, next_expr)) Loc.dummy_position
     | _ ->
        let v_expr =
          mk_expr (Eident (mk_id Loc.dummy_position id_s)) Loc.dummy_position in
        let n_e = id_change e id_s exists_id.id_str in
        let equal_expr = mk_expr (Eequals (v_expr, n_e)) Loc.dummy_position in
        let n_pre = id_change pre id_s exists_id.id_str in
        let exists_expr =
          mk_expr (Eand (equal_expr, n_pre)) Loc.dummy_position in
        mk_expr (Eexists (exists_id, exists_expr)) Loc.dummy_position
     end
  | _ -> expr
