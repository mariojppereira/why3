%{
    open While_ast
    open Why3

    let floc s e = Loc.extract (s,e)
%}

%token <string> ID
%token <int> CONST
%token SKIP
%token ASSIGN
%token IF
%token ELSE
%token SEMI_COLON
(* %token RETURN *)
%token LET
%token IN
%token EQUALS
%token LEFTPAR
%token RIGHTPAR
%token LEFTBRC
%token RIGHTBRC
%token LOGIC_AND
%token LOGIC_OR
%token EOF
%token COMMA
%token REQUIRES
%token NOT
%token PLUS
%token MINUS
%token LT
%token GT
%token LTGT
%token L_EQ
%token G_EQ
%token DOT
%token TRUE
%token FALSE
%token EXISTS
%token DIV
%token TIMES
%token LEFTSQ
%token RIGHTSQ
%token OLD

(* %nonassoc prec_no_else
 * %nonassoc ELSE *)
%nonassoc NOT
%left PLUS
%left MINUS
%left TIMES
%left DIV
%left LT
%left GT
%left L_EQ
%left G_EQ
%left LOGIC_AND
%left LOGIC_OR
%left LTGT
%nonassoc no_semicolon
%left SEMI_COLON


%start <While_ast.contract_expr option> prog

%%

prog:
  | EOF { None }
  | e = contract_expr EOF { Some e }
;

pre_expr:
  | REQUIRES ; LEFTBRC ; e = expr ; RIGHTBRC
     { e }

contract_expr:
  | pre_list = pre_expr* ; LEFTBRC ; body = seq_expr ; RIGHTBRC
     { mk_contract_expr body pre_list (floc $startpos $endpos) }

seq_expr:
  | e1 = expr ; SEMI_COLON ; e2 = seq_expr
    { mk_expr (Eseq (e1, e2)) (floc $startpos $endpos) }
  | e1 = expr; %prec no_semicolon
    { e1 }
  | e = expr ; SEMI_COLON
    { e }

identifier:
  | id = ID {mk_id (floc $startpos $endpos) id}
  | id1 = ID ; DOT ; id2 = ID
     { mk_record_app_id (floc $startpos $endpos) id1 id2 }
  | id = ID ; LEFTSQ ; id1 = ID ; RIGHTSQ
     { mk_array_app_id (floc $startpos $endpos) id id1 }
  | LEFTPAR ; OLD ; id = ID ; RIGHTPAR
     { mk_old_id (floc $startpos $endpos) id }
  (* | id = ID ; DOT ; id_list = separated_list (DOT, identifier)
   *   { Eident (mk_id_from_list (floc $startpos $endpos) id id_list) } *)
    
expr:
  | _expr { mk_expr $1 (floc $startpos $endpos)}
  | LEFTPAR ; e = expr ; RIGHTPAR
    { e }

_expr:
  | SKIP  { Eskip }
  | TRUE  { Etrue }
  | FALSE { Efalse }
  | LET ; ident = ID ; EQUALS ; e1 = expr ; IN ; e2 = seq_expr
    { Elet ((mk_id (floc $startpos $endpos) ident), e1, e2) }
  | IF ; bexp = expr ; LEFTBRC ; e1 = seq_expr ; RIGHTBRC ; ELSE ; LEFTBRC ; e2 = seq_expr ; RIGHTBRC
    { Eif (bexp, e1, e2) }
  | IF ; bexp = expr ; LEFTBRC ; e = seq_expr ; RIGHTBRC ;
    { Eif (bexp, e, mk_expr Eskip (floc $startpos $endpos))  }
  | id = identifier ; ASSIGN ; e = seq_expr
    { Eassign (mk_id (floc $startpos $endpos) id.id_str ,e) }
  | ident = identifier
    { Eident (mk_id (floc $startpos $endpos) ident.id_str)}
  | const = CONST
    { Econst const }
  | ident = ID ; LEFTPAR ; e_list = separated_list(COMMA,expr) ; RIGHTPAR
    { Eapp (mk_id (floc $startpos $endpos) ident,e_list) }
  | NOT (* ;  LEFTPAR *) ; e = expr (* ; RIGHTPAR *)
    { Enot e }
  | EXISTS ; id = ID ; DOT ; e = seq_expr
    { Eexists (mk_id (floc $startpos $endpos) id, e) }
  | c1 = expr ; PLUS ; c2 = expr
    { Emath_app (c1 ,mk_id (floc $startpos $endpos) "+", c2) }
  | c1 = expr ; MINUS ; c2 = expr
    { Emath_app (c1 ,mk_id (floc $startpos $endpos) "-", c2) }
  | c1 = expr ; TIMES ; c2 = expr
    { Emath_app (c1 ,mk_id (floc $startpos $endpos) "*", c2) }
  | c1 = expr ; DIV ; c2 = expr
    { Emath_app (c1 ,mk_id (floc $startpos $endpos) "/", c2) }
  | c1 = expr ; LT ; c2 = expr
    { Emath_app (c1 ,mk_id (floc $startpos $endpos) "<", c2) }
  | c1 = expr ; GT ; c2 = expr
    { Emath_app (c1 ,mk_id (floc $startpos $endpos) ">", c2) }
  | c1 = expr ; L_EQ ; c2 = expr
    { Emath_app (c1 ,mk_id (floc $startpos $endpos) "<=", c2) }
  | c1 = expr ; G_EQ ; c2 = expr
    { Emath_app (c1 ,mk_id (floc $startpos $endpos) ">=", c2) }
  | c1 = expr ; LTGT ; c2 = expr
    { Emath_app (c1 ,mk_id (floc $startpos $endpos) "<>", c2) }
  | e1 = expr ; LOGIC_AND ; e2 = expr
    { Eand (e1,e2) }
  | e1 = expr ; LOGIC_OR ; e2 = expr
    { Eor (e1,e2) }
  | LEFTSQ ; e_list = separated_list(COMMA,expr) ; RIGHTSQ
    { Etuple e_list }
;
