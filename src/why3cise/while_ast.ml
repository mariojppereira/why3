open Why3

type ident = {
  id_str : string;
  id_loc : Loc.position;
}

type expr = {
  expr_desc : expr_desc;
  expr_loc  : Loc.position;
}

and expr_desc =
  | Eskip
  | Etrue
  | Efalse
  | Elet        of ident * expr * expr
  | Eident      of ident
  | Econst      of int
  | Eseq        of expr * expr
  | Eapp        of ident * expr list
  | Eif         of expr * expr * expr
  | Eassign     of ident * expr
  | Eand        of expr * expr
  | Eor         of expr * expr
  | Enot        of expr
  | Eexists     of ident * expr
  | Eequals     of expr * expr
  | Emath_app   of expr * ident * expr
  | Erecord_app of ident * ident list
  | Etuple      of expr list

type contract_expr = {
  contract_e   : expr;
  contract_pre : expr list;
  contract_loc : Loc.position;
}

let mk_id loc str =
  { id_str = str ; id_loc = loc ;}

let mk_expr desc loc =
  { expr_desc = desc ; expr_loc = loc ;}

let mk_contract_expr expr pre_list loc =
  { contract_e = expr ; contract_pre = pre_list ; contract_loc = loc ;}

let mk_id_from_list loc id id_list =
  let n_id = List.fold_left (fun acc a ->
                 (acc ^ ".") ^ a.id_str) id id_list in
  { id_str = n_id ; id_loc = loc ;}

let mk_record_app_id loc id1 id2 =
  { id_str = (id1 ^ ".") ^ id2 ; id_loc = loc ;}

let mk_array_app_id loc id1 id2 =
  { id_str = ((id1 ^ "[") ^ id2) ^ "]" ; id_loc = loc ;}

let mk_old_id loc id =
  { id_str = ("(old" ^ id) ^ ")" ; id_loc = loc ;}

    (** Pretty-printing *)

open Format

let rec pp_list pp_elt pp_sep fmt list =
  match list with
  | []       -> ()
  | [hd]     ->
     pp_elt fmt hd
  | hd :: rl ->
     pp_elt fmt hd;
     pp_sep fmt ();
     pp_list pp_elt pp_sep fmt rl

(* let rec pp_str_list pp_elt pp_sep fmt list =
 *   match list with
 *   | []       -> ()
 *   | [hd]     ->
 *      pp_elt fmt hd
 *   | hd :: rl ->
 *      pp_elt fmt hd;
 *      pp_sep fmt ();
 *      pp_list pp_elt pp_sep fmt rl *)

let comma   fmt () = fprintf fmt ",@ "
let newline fmt () = fprintf fmt "@\n"
let dot     fmt () = fprintf fmt ".@"

let pp_string fmt str =
  fprintf fmt "%s" str

let rec pp_expr fmt {expr_desc = desc} =
  match desc with
  | Elet (x, e1, e2) ->
     fprintf fmt "let %s = %a in@ %a" x.id_str pp_expr e1 pp_expr e2
  | Eseq (e1, e2) ->
     fprintf fmt "%a;@\n%a" pp_expr e1 pp_expr e2
  | Eident id ->
     fprintf fmt "%s" id.id_str
  | Econst c ->
     fprintf fmt "%d" c
  | Eassign (id, e) ->
     fprintf fmt "%s <- %a" id.id_str pp_expr e
  | Eand (e1, e2) ->
     fprintf fmt "%a && %a" pp_expr e1 pp_expr e2
  | Eor (e1, e2) ->
     fprintf fmt "%a || %a" pp_expr e1 pp_expr e2
  | Enot e ->
     fprintf fmt "not (%a)" pp_expr e
  | Eequals (e1, e2) ->
     fprintf fmt "%a = %a" pp_expr e1 pp_expr e2
  | Eexists (id, e) ->
     fprintf fmt "exists %s . %a" id.id_str pp_expr e
  | Eapp (id, arg_list) ->
     fprintf fmt "%s (%a)" id.id_str (pp_list pp_expr comma) arg_list
  | Eif (b, e1 ,e2) ->
     fprintf fmt "if %a { %a } else { %a }" pp_expr b pp_expr e1 pp_expr e2
  | Eskip | Etrue ->
     fprintf fmt "true"
  | Efalse ->
     fprintf fmt "false"
  | Emath_app (e1,id,e2) ->
     fprintf fmt "%a %s %a" pp_expr e1 id.id_str pp_expr e2
  | Erecord_app (id, arg_list) ->
     let str_list = List.fold_left (fun acc a ->
                        a.id_str :: acc) [] arg_list in
     fprintf fmt "%s (%a)" id.id_str (pp_list pp_string dot) str_list
  | Etuple e_list ->
     fprintf fmt "(%a)" (pp_list pp_expr comma) e_list
    
let pp_contract_expr fmt {contract_e = e; contract_pre = pre_list} =
  let pp_requires fmt pre =
    fprintf fmt "requires { %a }" pp_expr pre in
  fprintf fmt "%a@\n%a" (pp_list pp_requires newline) pre_list pp_expr e

let pp_program ce =
  eprintf "%a@." pp_contract_expr ce
