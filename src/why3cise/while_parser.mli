
(* The type of tokens. *)

type token = 
  | TRUE
  | TIMES
  | SKIP
  | SEMI_COLON
  | RIGHTSQ
  | RIGHTPAR
  | RIGHTBRC
  | REQUIRES
  | PLUS
  | OLD
  | NOT
  | MINUS
  | L_EQ
  | LTGT
  | LT
  | LOGIC_OR
  | LOGIC_AND
  | LET
  | LEFTSQ
  | LEFTPAR
  | LEFTBRC
  | IN
  | IF
  | ID of (string)
  | G_EQ
  | GT
  | FALSE
  | EXISTS
  | EQUALS
  | EOF
  | ELSE
  | DOT
  | DIV
  | CONST of (int)
  | COMMA
  | ASSIGN

(* This exception is raised by the monolithic API functions. *)

exception Error

(* The monolithic API. *)

val prog: (Lexing.lexbuf -> token) -> Lexing.lexbuf -> (While_ast.contract_expr option)

module MenhirInterpreter : sig
  
  (* The incremental API. *)
  
  include MenhirLib.IncrementalEngine.INCREMENTAL_ENGINE
    with type token = token
  
end

(* The entry point(s) to the incremental API. *)

module Incremental : sig
  
  val prog: Lexing.position -> (While_ast.contract_expr option) MenhirInterpreter.checkpoint
  
end
