WHY3
====

Why3 is a platform for deductive program verification. It provides
a rich language for specification and programming, called WhyML, and
relies on external theorem provers, both automated and interactive,
to discharge verification conditions. Why3 comes with a standard
library of logical theories (integer and real arithmetic, Boolean
operations, sets and maps, etc.) and basic programming data structures
(arrays, queues, hash tables, etc.). A user can write WhyML programs
directly and get correct-by-construction OCaml programs through an
automated extraction mechanism. WhyML is also used as an intermediate
language for the verification of C, Java, or Ada programs.

PROJECT HOME
------------

http://why3.lri.fr/

https://gitlab.inria.fr/why3/why3

DOCUMENTATION
-------------

The documentation (a tutorial and a reference manual) is in the file
[doc/manual.pdf](http://why3.lri.fr/manual.pdf) or online at
http://why3.lri.fr/doc/.

Various examples can be found in the subdirectories [stdlib/](stdlib)
and [examples/](examples).

Mailing list (Why3 Club):
  http://lists.gforge.inria.fr/mailman/listinfo/why3-club

Bug Tracking System:
  https://gitlab.inria.fr/why3/why3/issues

COPYRIGHT
---------

This program is distributed under the GNU LGPL 2.1. See the enclosed
file [LICENSE](LICENSE).

The files [src/util/extmap.ml{i}](src/util/extmap.mli) are derived from the
sources of OCaml 3.12 standard library, and are distributed under the GNU
LGPL version 2 (see file [OCAML-LICENSE](OCAML-LICENSE)).

Icon sets for the graphical interface of Why3 are subject to specific
licenses, some of them may forbid commercial usage. These specific
licenses are detailed in files [share/images/\*/\*.txt](share/images).

INSTALLATION
------------

See the file [INSTALL.md](INSTALL.md).

STACY
=====

Stacy is a static analysis toolchain that checks conflicts in
distributed appllications. Below we explain how it works, and how it
can be applied over a set of provided case studies. These case studies
are present in `src/tools/stacy_examples/`.

EXECUTION
---------

To execute Stacy over an application file (`test.mlw` for example), without
providing a token system one must execute the following command:

```
  $ why3 stacy test.mlw -L .
```

To execute Stacy over an application file (`test.mlw` for example), providing
a token system (`token.txt` for example) one must execute the following command:

```
  $ why3 stacy test.mlw --tokens=token.txt -L .
```

After Stacy's execution, a file called `test__stacy.mlw` is created, where
the generated analysis functions are provided.

BANK APPLICATION
----------------

For this application we provide the application file (`bank.mlw`), and also
the two token systems present in the paper, in Section 3.2. The token systems
are represented in `bank_unrefined_tokens.txt` and `bank_refined_tokens.txt`.

To execute Stacy over `bank.mlw` without a token system one must execute
the following command:

```
  $ why3 stacy bank.mlw -L .
```

To execute Stacy over `bank.mlw` providing the unrefined token system,
one must execute the following command:

```
  $ why3 stacy bank.mlw --tokens=bank_unrefined_tokens.txt -L .
```

To execute Stacy over `bank.mlw` providing the refined token system,
one must execute the following command:

```
  $ why3 stacy bank.mlw --tokens=bank_refined_tokens.txt -L .
```

The results of executing Stacy for this application are then present
in a file called `bank_stacy.mlw`.

COURSEWARE APPLICATION WITH CONFLICTS
-------------------------------------

For this application we provide the application file (`courseware.mlw`) and also
two token systems in files `courseware_unrefined_tokens.txt` and
`courseware_refined_tokens.txt` as presented in Section 4.2.

To execute Stacy over `bank.mlw` without a token system one must execute
the following command:

```
  $ why3 stacy courseware.mlw -L .
```

To execute Stacy over `bank.mlw` providing the unrefined token system,
one must execute the following command:

```
  $ why3 stacy courseware.mlw --tokens=courseware_unrefined_tokens.txt -L .
```

To execute Stacy over `bank.mlw` providing the refined token system,
one must execute the following command:

```
  $ why3 stacy courseware.mlw --tokens=courseware_refined_tokens.txt -L .
```

The results of executing Stacy for this application are then present
in a file called `courseware_stacy.mlw`.

COURSEWARE APPLICATION WITH NO CONFLICTS
----------------------------------------

This application is similar to the previous one, with the difference that this
onehas a CRDT included, thus removing a commutativity issue from the previous
application. For this case study we present the application file
(`courseware_no_conflicts.mlw`), the same token system presented above in
`courseware_tokens.txt` as well as the CRDT file (`remove_wins_set.mlw`).

To execute Stacy over `bank.mlw` without a token system one must execute the
following command:

```
  $ why3 stacy courseware_no_conflicts.mlw -L .
```

To execute Stacy over bank.mlw providing the unrefined token system, one must
execute the following command:

```
  $ why3 stacy courseware_no_conflicts.mlw --tokens=courseware_unrefined_tokens.txt -L .
```

To execute Stacy over `bank.mlw` providing the refined token system, one must
execute the following command:

```
  $ why3 stacy courseware_no_conflicts.mlw --tokens=courseware_refined_tokens.txt -L .
```

The results of executing Stacy for this application are then present in a file
called `courseware_no_conflicts_stacy.mlw`.